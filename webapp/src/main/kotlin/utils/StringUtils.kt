package utils

fun getInitials(nickName: String): String {
    val splitNames = nickName.split(" ")
    var initials: String
    if (splitNames.size > 1) {
        initials  = splitNames[0].first().toString()
        initials += splitNames[1].first().toString()
    } else {
        initials  = if (nickName.length > 2) nickName.substring(0, 2) else nickName
    }

    return initials.toUpperCase()
}