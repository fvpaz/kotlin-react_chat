package views

import kotlinx.browser.document
import kotlinx.html.ButtonType
import kotlinx.html.js.onClickFunction
import kotlinx.html.id
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onKeyUpFunction
import org.w3c.dom.*
import react.*
import kotlin.js.Json
import react.dom.*
import react.dom.button
import react.dom.h2
import react.dom.hr
import react.dom.input
import socket
import views.react.component.chatMsgList
import views.react.component.divOnlineUsers
import kotlin.js.json

interface chatWindowProps: RProps {
    var nickNameGlobal: String
}
val chatWindowComponent = functionalComponent<chatWindowProps> { props ->
    val (mensajes, setMensajes) = useState(emptyArray<Json>()) //{ nick, msg, type}
    val (users, setUsers) = useState(emptyArray<Json>())

    val (msg,setMsg) = useState("")

    fun showOnlineUsers(userList: Array<Json>) { //Willmount
        userList.let {
            setUsers(userList)
        }
    }

    useEffect(listOf(users)) {
        socket.on("loginUser") { data ->
            console.log("OnloginUser")
            val noOfUsers = data["numOfUsers"] as Int
            val item = getMsgJson("", getParticipantsMessage(noOfUsers), "log")
            val items = mensajes.plus(item) //plusElement(item)

            setMensajes(items)
            showOnlineUsers(data["usersList"]) //set list users
        }
    }

    useEffect(listOf(users)) {
        socket.on("joinUser") { data ->
            console.log("onJoinUser")
            val item = getMsgJson("", "${data["nickName"]} has joined", "log")
            val items = mensajes.plusElement(item)
            //setMensajes(items)
            showOnlineUsers(data["usersList"]) //set list users
        }
    }

    useEffect(listOf(users)) {
        socket.on("user_left") { data ->
            val numOfUsers = data["numOfUsers"]
            var items = mensajes.plusElement(getMsgJson("", "${data["nickName"]} left", "log"))
            items = items.plusElement(getMsgJson("", getParticipantsMessage(numOfUsers), "log"))
            setMensajes(items);
            showOnlineUsers(data["usersList"])
        }
    }

    fun logMessageFromMe(nickName: String, message: String) {
        val items = mensajes.plusElement(getMsgJson(nickName,message,"sentMessages")) //ArrayList<Message>
        console.log("items:${items[0]}")
        setMensajes(items)
    }

    fun logMessage(nickName: String, message: String) {
        val items = mensajes.plusElement(getMsgJson(nickName, message,"receivedMessages"))
        console.log("items:${items[0]}")
        setMensajes(items)
    }

    fun onSendMessageClicked() {
        if (msg.isNotBlank()) {
            logMessageFromMe(props.nickNameGlobal, msg)
            socket.emit("new_message", msg)
        }
    }

    fun showNewMessage(data: Json) {
        console.log("receive message")
        logMessage(message = data["message"] as String, nickName = data["nickName"] as String)
    }

    useEffect {
        socket.on("new_message") { data ->
            console.log("onNewMsg")
            showNewMessage(data)
        }
    }

    div(classes = "chatWindow") {
        h2(classes = "chatWelcomeMessage") {
            +"Welcome to Kotlin React chat app"
        }
        hr { }
        div {
            attrs.id = "leftDiv"

            chatMsgList {  //Chatmessages -> ul
                this.mensajes = mensajes
            }

            div {
                input(classes = "chatMessageInput") {
                    attrs.id = "chatInputBox"
                    attrs.placeholder = "Enter your message here and hit ENTER"
                    attrs.maxLength = 100.toString()
                    attrs.onChangeFunction  =  {
                        val input = it.target as HTMLInputElement?
                        input?.let {
                            setMsg(it.value)
                        }
                    }
                    attrs.onKeyUpFunction = {
                        it.preventDefault()
                        val event = it.asDynamic()
                        if (event.key == "Enter" || event.keyCode === 13) onSendMessageClicked()
                    }
                }
                button(classes = "sendMessageButton") {
                    +"Send Message"
                    attrs.type = ButtonType.button
                    attrs.onClickFunction = {  onSendMessageClicked() }
                }
            }
        }
        divOnlineUsers {
            console.log(users)
            userList = users
        }
        div(classes = "clearBoth") { }
    }
}

fun addNewUsers(data: Json) {
    val nickName = data["nickName"].toString();
    //getUserListItem(onlineUsersList, nickName)
}

fun showNewUserJoined(data: Json) { //TODO
}

private fun getParticipantsMessage(noOfUsers: Int) =
        if (noOfUsers == 1) "There is $noOfUsers participant" else "There are $noOfUsers participants"

fun getMsgJson(nick: String = "", msg: String, type: String): Json {
    return if (nick.isBlank())
        json("msg" to msg, "type" to type)
    else json("nickName" to nick,
            "msg" to msg,
            "type" to type)
}

//class ChatWindow(val callback: (String) -> Unit) { }