package views.react.component

import kotlinx.html.id
import react.*
import react.dom.*
import utils.getInitials
import kotlin.js.Json

interface onlineUsersProps : RProps{
    var userList: Array<Json>
}
val onlineUsers = functionalComponent<onlineUsersProps> { props ->
    div {
        attrs.id = "rightDiv"
        h4 {
            +"Online users"
        }
        hr { }
        ul(classes = "onlineUserList") {
            attrs.id = "onlineUsersList"
            props.userList.mapIndexed { index, user ->
                li(classes = "onlineUserListItem") {
                    val nickName = user["nickname"].toString()
                    span(classes = "ringInitials") {
                        +getInitials(nickName)
                    }
                    span(classes = "onlineUserListItemText") {
                        +nickName
                    }
                }
            }
        }
    }
}

fun RBuilder.divOnlineUsers(props: onlineUsersProps.() -> Unit): ReactElement {
    return child(onlineUsers) {
        this.attrs(props)
    }
}