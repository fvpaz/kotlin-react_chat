package views.react.component

import kotlinx.html.id
import org.w3c.dom.events.Event
import react.*
import react.dom.*
import utils.getInitials

data class Message(val nickName: String = "", val msg: String, val type: String)
external interface ChatMsgListProps : RProps {
    var mensajes: Array<dynamic> //Json Array = Mensajes
    var onMessageSent: (Event) -> Unit
}

interface itemBaseProps : RProps {
    var key: String
    var mensaje: String
}

interface itemMsgProps: itemBaseProps{
    var type:String
    var nickName:String
}

val chatMsgList = functionalComponent<ChatMsgListProps> { props ->
    ul(classes = "chatMessages") {
        attrs.id = "chatMessages"
        props.mensajes.mapIndexed { index, msg ->
            when (msg["type"]) {
                "log" -> logitem {
                    key = index.toString()
                    mensaje = msg["msg"]
                }
                "sentMessages" , "receivedMessages" -> itemMsg {
                    key = index.toString()
                    mensaje= msg["msg"]
                    type = msg["type"]
                    nickName = msg["nickName"]
                }
                else -> console.log("fail load MsgList!")
            }
        }
    }
}

val itemMsg = functionalComponent<itemMsgProps> { props ->
    li {
        attrs.key = props.key
        div(classes = props.type) {
            if (props.type == "sentMessages") {
                span(classes = "chatMessage") {
                    +props.mensaje
                }
                span(classes = "filledInitials") {
                    +getInitials(props.nickName)
                }
            } else if (props.type.equals("receivedMessages")){
                span(classes = "filledInitials") {
                    +getInitials(props.nickName)
                }
                span(classes = "chatMessage") {
                    +props.mensaje
                }
            }
        }
    }
}

val itemLog = functionalComponent<itemBaseProps> { props ->
    li(classes = "log") {
        attrs.key = props.key
        p {
            +props.mensaje
        }
    }
}

fun RBuilder.chatMsgList(props: ChatMsgListProps.() -> Unit): ReactElement {
    return child(chatMsgList) {
        this.attrs(props)
    }
}

fun RBuilder.logitem(props: itemBaseProps.() -> Unit): ReactElement {
    return child(itemLog) {
        this.attrs(props)
    }
}

fun RBuilder.itemMsg(props: itemMsgProps.() -> Unit) :ReactElement {
    return child(itemMsg){
        this.attrs(props)
    }
}