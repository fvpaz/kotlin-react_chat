package views

import kotlinx.browser.window
import kotlinx.html.id
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.onInputFunction
import kotlinx.html.js.onKeyUpFunction
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.Event
import react.*
import react.dom.*
import org.w3c.dom.Window
import socket
import kotlin.js.json
import kotlin.properties.Delegates

private lateinit var email: String
private lateinit var nickName: String

interface loginProps : RProps {
    var setUser: RSetState<String>
}

val loginWindow = functionalComponent<loginProps> { props ->
    fun onInput(): (Event) -> Unit {
        return {
            val input = it.currentTarget as HTMLInputElement
            when (input.id) {
                "nickName" -> nickName = input.value
                "emailId" -> email = input.value
            }
        }
    }

    fun onLogin() {
        if (!nickName.isBlank()) {
            socket.emit("join", nickName ) { error, succes   ->
                val msg = error as String

                when{
                    msg.isNotBlank() -> window.alert(error)
                    succes           -> props.setUser(nickName)
                }
//                if(msg.isNotBlank())
//                    window.alert(error)
//                if (succes) asSucces = succes
            }
        }
    }

    div {
        attrs.id = "loginDiv"
        h3(classes = "title") {
            +"Welcome to Kotlin chat app"
        }
        input(classes = "nickNameInput") {
            attrs.id = "nickName"
            attrs.onInputFunction = onInput()
            attrs.onKeyUpFunction = {
                it.preventDefault()
                val input = it.currentTarget as HTMLInputElement
                nickName = input.value
                email = input.value
                val event = it.asDynamic()
                if (event.key === "Enter" || event.keyCode === 13)
                    onLogin()
            }
            attrs.maxLength = 18.toString()
            attrs.placeholder = "Tip your Nickname and hit enter"
        }
        button(classes = "loginButton") {
            +"Login"
            attrs.onClickFunction = {
                onLogin()
            }
        }
    }
}