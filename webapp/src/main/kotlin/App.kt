import kotlinx.browser.document
import react.RProps
import react.child
import react.dom.render
import react.functionalComponent
import react.useState
import views.chatWindowComponent
import views.loginWindow

val ENDPOINT = "localhost:8000"
val socket: dynamic = js("io('$ENDPOINT')")
fun main() {
    val App = functionalComponent<RProps> { props ->
        val (user, setUser) = useState("")

        if (user.isNullOrBlank()) { //<Login onUserLoggedIn=setUser/>
            child(loginWindow) {
                attrs.setUser = setUser
            }
        } else {
            child(chatWindowComponent) {
                attrs.nickNameGlobal = user
            }
        }
    }

    render(document.getElementById("container")) {
        child(App) {}
    }
}