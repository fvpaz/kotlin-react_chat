import org.jetbrains.kotlin.gradle.tasks.*
import org.jetbrains.kotlin.gradle.dsl.*

group = "org.example"
version = "1.0.0"

repositories {
    maven("https://kotlin.bintray.com/kotlin-js-wrappers")
}

val kotlinVersion   = "1.4.0-rc"
val buildKotlinlibs = "pre.109-kotlin"
val reactVersion    = "16.13.1"
val reactKotlin     = "$reactVersion-$buildKotlinlibs-$kotlinVersion"
val styleVersion    = "1.0.0-$buildKotlinlibs-$kotlinVersion"
dependencies {
    implementation(kotlin("stdlib-js"))
    val htmlVersion = "0.7.1"
    implementation("io.socket:socket.io-client:1.0.0") {
        // exclude org.json which is provided by Android
        exclude("org.json", "json")
    }

    //React, React DOM + Wrappers
    implementation("org.jetbrains:kotlin-react:$reactKotlin") //https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react
    implementation("org.jetbrains:kotlin-react-dom:$reactKotlin")
    //Kotlin Styled (CSS)
    implementation("org.jetbrains:kotlin-styled:$styleVersion")  //https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-styled
}

kotlin {
    js(LEGACY){
        browser {
            dceTask {
                keep += "kotlin_react_chat_app-webapp.main"
            }

            webpackTask {
                cssSupport.enabled = true
            }

            runTask {
                cssSupport.enabled = true
            }

            testTask {
                useKarma {
                    useChromeHeadless()
                    webpackConfig.cssSupport.enabled = true
                }
            }
        }
        binaries.executable()
    }

    sourceSets["main"].kotlin.srcDirs("src")
}
//apply(from ="sourceSet.gradle")

//tasks.compileKotlinJs{
//    getkotlinOptions(kotlinOptions)
//}

tasks {
    named("compileKotlinJs", Kotlin2JsCompile::class) {
        getkotlinOptions(kotlinOptions)
    }
}

fun getkotlinOptions(kotlinOptions: KotlinJsOptions) {
    kotlinOptions.metaInfo = true
    kotlinOptions.outputFile = "${projectDir}/js/webapp.js"
    kotlinOptions.main = "call"
    kotlinOptions.moduleKind = "commonjs"


    // these two options enable browser debugging
    // disable for production to reduce file size
    kotlinOptions.sourceMap = true
//    kotlinOptions.sourceMapEmbedSources = "always"
}

tasks.register<Copy>("copyResources") {
    from(kotlin.sourceSets["main"].resources.srcDirs)
    into(file(buildDir.path + "/js"))
}

tasks.build{
    doLast{
        copy {
            from("$projectDir/build/distributions/webapp.js", "$projectDir/build/distributions/webapp.js.map")
            into("$projectDir/lib/js")
        }
    }
}

//tasks.build {
//    doLast {
//        val implLibs by configurations.creating{
//            extendsFrom(configurations["implementation"])
//        }
//
//        configurations["implLibs"].forEach {
//            copy {
//                includeEmptyDirs = false
//
//                println(it.absolutePath)
//                from(zipTree(it.absolutePath))
//                into("${projectDir}/lib/kotlin")
//                include { fileTreeElement ->
//                    val path = fileTreeElement.path
//                    if (path.endsWith(".js"))
//                        println(path)
//                    path.endsWith(".js") && (path.startsWith("META-INF/resources/") || !path.startsWith("META-INF/"))
//                }
//            }
//        }
//    }
//}

tasks.register<Copy>("syncCompileKotlinJs") {
    val compile = tasks.withType(Kotlin2JsCompile::class).named("compileKotlinJs")
    into("${projectDir}/js/webapp.js") {
        from(
            compile.map {
                    it.outputFile.parentFile
            }
        )
    }
    into("$buildDir")
    dependsOn(compile)
}