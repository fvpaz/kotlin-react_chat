(function (_, Kotlin) {
  'use strict';
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var LinkedHashMap_init = Kotlin.kotlin.collections.LinkedHashMap_init_q3lmfv$;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Unit = Kotlin.kotlin.Unit;
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var get_lastIndex = Kotlin.kotlin.collections.get_lastIndex_55thoc$;
  var Regex_init = Kotlin.kotlin.text.Regex_init_61zpoe$;
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_287e2$;
  var StringBuilder_init = Kotlin.kotlin.text.StringBuilder_init;
  var unboxChar = Kotlin.unboxChar;
  var CharRange = Kotlin.kotlin.ranges.CharRange;
  var toChar = Kotlin.toChar;
  var iterator = Kotlin.kotlin.text.iterator_gw00vp$;
  var toBoxedChar = Kotlin.toBoxedChar;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var equals = Kotlin.equals;
  var startsWith = Kotlin.kotlin.text.startsWith_sgbm27$;
  var Enum = Kotlin.kotlin.Enum;
  var throwISE = Kotlin.throwISE;
  var IllegalArgumentException_init = Kotlin.kotlin.IllegalArgumentException_init_pdl1vj$;
  var math = Kotlin.kotlin.math;
  var endsWith_0 = Kotlin.kotlin.text.endsWith_sgbm27$;
  var IntRange = Kotlin.kotlin.ranges.IntRange;
  var Math_0 = Math;
  var IllegalStateException_init = Kotlin.kotlin.IllegalStateException_init_pdl1vj$;
  var joinToString_0 = Kotlin.kotlin.collections.joinToString_fmv235$;
  var Any = Object;
  var throwCCE = Kotlin.throwCCE;
  var PropertyMetadata = Kotlin.PropertyMetadata;
  var toString = Kotlin.toString;
  var toSet = Kotlin.kotlin.collections.toSet_us0mfu$;
  var emptySet = Kotlin.kotlin.collections.emptySet_287e2$;
  Animations.prototype = Object.create(StyleList.prototype);
  Animations.prototype.constructor = Animations;
  BoxShadows.prototype = Object.create(StyleList.prototype);
  BoxShadows.prototype.constructor = BoxShadows;
  Transforms.prototype = Object.create(StyleList.prototype);
  Transforms.prototype.constructor = Transforms;
  Transitions.prototype = Object.create(StyleList.prototype);
  Transitions.prototype.constructor = Transitions;
  function hyphenize$lambda(it) {
    var $receiver = StringBuilder_init();
    var tmp$;
    tmp$ = iterator(it);
    while (tmp$.hasNext()) {
      var element = unboxChar(tmp$.next());
      var it_0 = toBoxedChar(element);
      var tmp$_0, tmp$_1;
      tmp$_0 = unboxChar(it_0);
      if ((new CharRange(65, 90)).contains_mef7kx$(tmp$_0)) {
        var tmp$_2 = String;
        var tmp$_3 = tmp$_2.fromCharCode;
        var $receiver_0 = unboxChar(it_0);
        tmp$_1 = '-' + tmp$_3.call(tmp$_2, toChar(String.fromCharCode($receiver_0 | 0).toLowerCase().charCodeAt(0)));
      } else
        tmp$_1 = it_0;
      $receiver.append_s8jyv4$(tmp$_1);
    }
    return $receiver.toString();
  }
  var hyphenize;
  function memoizeString$lambda(closure$map, closure$fn) {
    return function (it) {
      var closure$map_0 = closure$map;
      var closure$fn_0 = closure$fn;
      if (!closure$map_0.containsKey_11rb$(it)) {
        var value = closure$fn_0(it);
        closure$map_0.put_xwzc9p$(it, value);
      }return ensureNotNull(closure$map_0.get_11rb$(it));
    };
  }
  function memoizeString(fn) {
    var map = LinkedHashMap_init();
    return memoizeString$lambda(map, fn);
  }
  var ZERO;
  var LinearDimension$Companion_instance = null;
  var Align$initial_instance;
  var Align$inherit_instance;
  var Align$unset_instance;
  var Align$auto_instance;
  var Align$stretch_instance;
  var Align$center_instance;
  var Align$flexStart_instance;
  var Align$flexEnd_instance;
  var Align$baseline_instance;
  var JustifyContent$initial_instance;
  var JustifyContent$inherit_instance;
  var JustifyContent$unset_instance;
  var JustifyContent$center_instance;
  var JustifyContent$start_instance;
  var JustifyContent$end_instance;
  var JustifyContent$flexStart_instance;
  var JustifyContent$flexEnd_instance;
  var JustifyContent$left_instance;
  var JustifyContent$right_instance;
  var JustifyContent$baseline_instance;
  var JustifyContent$firstBaseline_instance;
  var JustifyContent$lastBaseline_instance;
  var JustifyContent$spaceBetween_instance;
  var JustifyContent$spaceAround_instance;
  var JustifyContent$spaceEvenly_instance;
  var JustifyContent$stretch_instance;
  var JustifyContent$safeCenter_instance;
  var JustifyContent$unsafeCenter_instance;
  var BackgroundRepeat$initial_instance;
  var BackgroundRepeat$inherit_instance;
  var BackgroundRepeat$unset_instance;
  var BackgroundRepeat$repeatX_instance;
  var BackgroundRepeat$repeatY_instance;
  var BackgroundRepeat$repeat_instance;
  var BackgroundRepeat$noRepeat_instance;
  var BackgroundAttachment$initial_instance;
  var BackgroundAttachment$inherit_instance;
  var BackgroundAttachment$unset_instance;
  var BackgroundAttachment$scroll_instance;
  var BackgroundAttachment$fixed_instance;
  var BackgroundAttachment$local_instance;
  var BackgroundClip$initial_instance;
  var BackgroundClip$inherit_instance;
  var BackgroundClip$unset_instance;
  var BackgroundClip$borderBox_instance;
  var BackgroundClip$paddingBox_instance;
  var BackgroundClip$contentBox_instance;
  var BackgroundClip$text_instance;
  var BackgroundOrigin$initial_instance;
  var BackgroundOrigin$inherit_instance;
  var BackgroundOrigin$unset_instance;
  var BackgroundOrigin$borderBox_instance;
  var BackgroundOrigin$paddingBox_instance;
  var BackgroundOrigin$contentBox_instance;
  var BorderCollapse$initial_instance;
  var BorderCollapse$inherit_instance;
  var BorderCollapse$unset_instance;
  var BorderCollapse$separate_instance;
  var BorderCollapse$collapse_instance;
  var BorderStyle$initial_instance;
  var BorderStyle$inherit_instance;
  var BorderStyle$unset_instance;
  var BorderStyle$none_instance;
  var BorderStyle$dotted_instance;
  var BorderStyle$dashed_instance;
  var BorderStyle$solid_instance;
  var BoxSizing$initial_instance;
  var BoxSizing$inherit_instance;
  var BoxSizing$unset_instance;
  var BoxSizing$contentBox_instance;
  var BoxSizing$borderBox_instance;
  var Clear$initial_instance;
  var Clear$inherit_instance;
  var Clear$unset_instance;
  var Clear$none_instance;
  var Clear$left_instance;
  var Clear$right_instance;
  var Clear$both_instance;
  var Color$Companion_instance = null;
  var ColumnGap$Companion_instance = null;
  var Contain$initial_instance;
  var Contain$inherit_instance;
  var Contain$unset_instance;
  var Contain$none_instance;
  var Contain$strict_instance;
  var Contain$content_instance;
  var Contain$size_instance;
  var Contain$layout_instance;
  var Contain$style_instance;
  var Contain$paint_instance;
  var Cursor$initial_instance;
  var Cursor$inherit_instance;
  var Cursor$unset_instance;
  var Cursor$auto_instance;
  var Cursor$default_instance;
  var Cursor$none_instance;
  var Cursor$contextMenu_instance;
  var Cursor$help_instance;
  var Cursor$pointer_instance;
  var Cursor$progress_instance;
  var Cursor$wait_instance;
  var Cursor$cell_instance;
  var Cursor$crosshair_instance;
  var Cursor$text_instance;
  var Cursor$verticalText_instance;
  var Cursor$alias_instance;
  var Cursor$copy_instance;
  var Cursor$move_instance;
  var Cursor$noDrop_instance;
  var Cursor$notAllowed_instance;
  var Cursor$grab_instance;
  var Cursor$grabbing_instance;
  var Cursor$colResize_instance;
  var Cursor$rowResize_instance;
  var Cursor$allScroll_instance;
  var Cursor$eResize_instance;
  var Cursor$nResize_instance;
  var Cursor$neResize_instance;
  var Cursor$nwResize_instance;
  var Cursor$sResize_instance;
  var Cursor$seResize_instance;
  var Cursor$swResize_instance;
  var Cursor$wResize_instance;
  var Cursor$ewResize_instance;
  var Cursor$nsResize_instance;
  var Cursor$neswResize_instance;
  var Cursor$nwseResize_instance;
  var Cursor$zoomIn_instance;
  var Cursor$zoomOut_instance;
  var Direction$initial_instance;
  var Direction$inherit_instance;
  var Direction$unset_instance;
  var Direction$ltr_instance;
  var Direction$rtl_instance;
  var Display$initial_instance;
  var Display$inherit_instance;
  var Display$unset_instance;
  var Display$block_instance;
  var Display$inline_instance;
  var Display$runIn_instance;
  var Display$flow_instance;
  var Display$flowRoot_instance;
  var Display$table_instance;
  var Display$flex_instance;
  var Display$grid_instance;
  var Display$subgrid_instance;
  var Display$listItem_instance;
  var Display$tableRowGroup_instance;
  var Display$tableHeaderGroup_instance;
  var Display$tableFooterGroup_instance;
  var Display$tableRow_instance;
  var Display$tableCell_instance;
  var Display$tableColumnGroup_instance;
  var Display$tableColumn_instance;
  var Display$tableCaption_instance;
  var Display$contents_instance;
  var Display$none_instance;
  var Display$inlineBlock_instance;
  var Display$inlineListItem_instance;
  var Display$inlineTable_instance;
  var Display$inlineFlex_instance;
  var Display$inlineGrid_instance;
  var FlexBasis$Companion_instance = null;
  var FlexWrap$initial_instance;
  var FlexWrap$inherit_instance;
  var FlexWrap$unset_instance;
  var FlexWrap$nowrap_instance;
  var FlexWrap$wrap_instance;
  var FlexWrap$wrapReverse_instance;
  var Float$initial_instance;
  var Float$inherit_instance;
  var Float$unset_instance;
  var Float$left_instance;
  var Float$right_instance;
  var Float$none_instance;
  var FontWeight$Companion_instance = null;
  var FontStyle$Companion_instance = null;
  var FlexDirection$initial_instance;
  var FlexDirection$inherit_instance;
  var FlexDirection$unset_instance;
  var FlexDirection$column_instance;
  var FlexDirection$columnReverse_instance;
  var FlexDirection$row_instance;
  var FlexDirection$rowReverse_instance;
  var Gap$Companion_instance = null;
  var GridAutoColumns$Companion_instance = null;
  var GridAutoFlow$Companion_instance = null;
  var GridAutoRows$Companion_instance = null;
  var GridColumn$Companion_instance = null;
  var GridColumnEnd$Companion_instance = null;
  var GridColumnGap$Companion_instance = null;
  var GridColumnStart$Companion_instance = null;
  var GridGap$Companion_instance = null;
  var GridRow$Companion_instance = null;
  var GridRowEnd$Companion_instance = null;
  var GridRowGap$Companion_instance = null;
  var GridRowStart$Companion_instance = null;
  var GridTemplate$Companion_instance = null;
  var GridTemplateAreas$Companion_instance = null;
  var GridTemplateColumns$Companion_instance = null;
  var GridTemplateRows$Companion_instance = null;
  var Grow$NONE_instance;
  var Grow$GROW_instance;
  var Grow$SHRINK_instance;
  var Grow$GROW_SHRINK_instance;
  var Hyphens$initial_instance;
  var Hyphens$inherit_instance;
  var Hyphens$unset_instance;
  var Hyphens$none_instance;
  var Hyphens$manual_instance;
  var Hyphens$auto_instance;
  var ListStyleType$initial_instance;
  var ListStyleType$inherit_instance;
  var ListStyleType$unset_instance;
  var ListStyleType$none_instance;
  var ListStyleType$disc_instance;
  var ListStyleType$circle_instance;
  var ListStyleType$square_instance;
  var ListStyleType$decimal_instance;
  var ObjectFit$initial_instance;
  var ObjectFit$inherit_instance;
  var ObjectFit$unset_instance;
  var ObjectFit$contain_instance;
  var ObjectFit$cover_instance;
  var ObjectFit$fill_instance;
  var ObjectFit$none_instance;
  var ObjectFit$scaleDown_instance;
  var Outline$initial_instance;
  var Outline$inherit_instance;
  var Outline$unset_instance;
  var Outline$none_instance;
  var Overflow$initial_instance;
  var Overflow$inherit_instance;
  var Overflow$unset_instance;
  var Overflow$visible_instance;
  var Overflow$hidden_instance;
  var Overflow$scroll_instance;
  var Overflow$auto_instance;
  var OverflowWrap$initial_instance;
  var OverflowWrap$inherit_instance;
  var OverflowWrap$unset_instance;
  var OverflowWrap$normal_instance;
  var OverflowWrap$breakWord_instance;
  var OverscrollBehavior$initial_instance;
  var OverscrollBehavior$inherit_instance;
  var OverscrollBehavior$unset_instance;
  var OverscrollBehavior$auto_instance;
  var OverscrollBehavior$contain_instance;
  var OverscrollBehavior$none_instance;
  var PointerEvents$initial_instance;
  var PointerEvents$inherit_instance;
  var PointerEvents$unset_instance;
  var PointerEvents$auto_instance;
  var PointerEvents$none_instance;
  var Position$initial_instance;
  var Position$inherit_instance;
  var Position$unset_instance;
  var Position$static_instance;
  var Position$relative_instance;
  var Position$absolute_instance;
  var Position$fixed_instance;
  var Position$sticky_instance;
  var RowGap$Companion_instance = null;
  var ScrollBehavior$initial_instance;
  var ScrollBehavior$inherit_instance;
  var ScrollBehavior$unset_instance;
  var ScrollBehavior$auto_instance;
  var ScrollBehavior$smooth_instance;
  var TextAlign$initial_instance;
  var TextAlign$inherit_instance;
  var TextAlign$unset_instance;
  var TextAlign$left_instance;
  var TextAlign$right_instance;
  var TextAlign$center_instance;
  var TextAlign$justify_instance;
  var TextAlign$justifyAll_instance;
  var TextAlign$start_instance;
  var TextAlign$end_instance;
  var TextAlign$matchParent_instance;
  var TableLayout$initial_instance;
  var TableLayout$inherit_instance;
  var TableLayout$unset_instance;
  var TableLayout$auto_instance;
  var TableLayout$fixed_instance;
  var TextOverflow$initial_instance;
  var TextOverflow$inherit_instance;
  var TextOverflow$unset_instance;
  var TextOverflow$clip_instance;
  var TextOverflow$ellipsis_instance;
  var TextTransform$initial_instance;
  var TextTransform$inherit_instance;
  var TextTransform$unset_instance;
  var TextTransform$capitalize_instance;
  var TextTransform$uppercase_instance;
  var TextTransform$lowercase_instance;
  var TextTransform$none_instance;
  var TextTransform$fullWidth_instance;
  var UserSelect$initial_instance;
  var UserSelect$inherit_instance;
  var UserSelect$unset_instance;
  var UserSelect$none_instance;
  var UserSelect$auto_instance;
  var UserSelect$text_instance;
  var UserSelect$contain_instance;
  var UserSelect$all_instance;
  var VerticalAlign$Companion_instance = null;
  var Visibility$initial_instance;
  var Visibility$inherit_instance;
  var Visibility$unset_instance;
  var Visibility$visible_instance;
  var Visibility$hidden_instance;
  var Visibility$collapse_instance;
  var WhiteSpace$initial_instance;
  var WhiteSpace$inherit_instance;
  var WhiteSpace$unset_instance;
  var WhiteSpace$normal_instance;
  var WhiteSpace$nowrap_instance;
  var WhiteSpace$pre_instance;
  var WhiteSpace$preWrap_instance;
  var WhiteSpace$preLine_instance;
  var WordBreak$initial_instance;
  var WordBreak$inherit_instance;
  var WordBreak$unset_instance;
  var WordBreak$normal_instance;
  var WordBreak$breakAll_instance;
  var WordBreak$breakWord_instance;
  var WordBreak$keepAll_instance;
  var WordWrap$initial_instance;
  var WordWrap$inherit_instance;
  var WordWrap$unset_instance;
  var WordWrap$normal_instance;
  var WordWrap$breakWord_instance;
  var Image$Companion_instance = null;
  function StyleList(delimiter) {
    this.delimiter_o62vis$_0 = delimiter;
    this.list_behuth$_0 = ArrayList_init();
  }
  StyleList.prototype.toString = function () {
    if (this.list_behuth$_0.isEmpty())
      return 'none';
    else
      return joinToString_0(this.list_behuth$_0, this.delimiter_o62vis$_0);
  };
  StyleList.prototype.clear = function () {
    this.list_behuth$_0.clear();
  };
  StyleList.prototype.plusAssign_11rb$ = function (item) {
    this.list_behuth$_0.add_11rb$(item);
  };
  StyleList.$metadata$ = {kind: Kind_CLASS, simpleName: 'StyleList', interfaces: []};
  function CSSProperty(default_0) {
    if (default_0 === void 0)
      default_0 = null;
    this.default_0 = default_0;
  }
  CSSProperty.prototype.getValue_jto6o9$ = function (thisRef, property) {
    var tmp$, tmp$_0;
    if ((tmp$ = this.default_0) != null) {
      var tmp$_1;
      if (!thisRef.declarations.containsKey_11rb$(property.callableName)) {
        var $receiver = thisRef.declarations;
        var key = property.callableName;
        var value = Kotlin.isType(tmp$_1 = tmp$(), Any) ? tmp$_1 : throwCCE();
        $receiver.put_xwzc9p$(key, value);
      }}return (tmp$_0 = thisRef.declarations.get_11rb$(property.callableName)) == null || Kotlin.isType(tmp$_0, Any) ? tmp$_0 : throwCCE();
  };
  CSSProperty.prototype.setValue_6qj5c4$ = function (thisRef, property, value) {
    var tmp$;
    var $receiver = thisRef.declarations;
    var key = property.callableName;
    var value_0 = Kotlin.isType(tmp$ = value, Any) ? tmp$ : throwCCE();
    $receiver.put_xwzc9p$(key, value_0);
  };
  CSSProperty.$metadata$ = {kind: Kind_CLASS, simpleName: 'CSSProperty', interfaces: []};
  var alignContent;
  var alignContent_metadata = new PropertyMetadata('alignContent');
  var alignItems;
  var alignItems_metadata = new PropertyMetadata('alignItems');
  var alignSelf;
  var alignSelf_metadata = new PropertyMetadata('alignSelf');
  function animation$lambda() {
    return new Animations();
  }
  var animation;
  var animation_metadata = new PropertyMetadata('animation');
  var background;
  var background_metadata = new PropertyMetadata('background');
  var backgroundAttachment;
  var backgroundAttachment_metadata = new PropertyMetadata('backgroundAttachment');
  var backgroundClip;
  var backgroundClip_metadata = new PropertyMetadata('backgroundClip');
  var backgroundColor;
  var backgroundColor_metadata = new PropertyMetadata('backgroundColor');
  var backgroundImage;
  var backgroundImage_metadata = new PropertyMetadata('backgroundImage');
  var backgroundOrigin;
  var backgroundOrigin_metadata = new PropertyMetadata('backgroundOrigin');
  var backgroundPosition;
  var backgroundPosition_metadata = new PropertyMetadata('backgroundPosition');
  var backgroundRepeat;
  var backgroundRepeat_metadata = new PropertyMetadata('backgroundRepeat');
  var backgroundSize;
  var backgroundSize_metadata = new PropertyMetadata('backgroundSize');
  var border;
  var border_metadata = new PropertyMetadata('border');
  var borderTop;
  var borderTop_metadata = new PropertyMetadata('borderTop');
  var borderRight;
  var borderRight_metadata = new PropertyMetadata('borderRight');
  var borderBottom;
  var borderBottom_metadata = new PropertyMetadata('borderBottom');
  var borderLeft;
  var borderLeft_metadata = new PropertyMetadata('borderLeft');
  var borderSpacing;
  var borderSpacing_metadata = new PropertyMetadata('borderSpacing');
  var borderRadius;
  var borderRadius_metadata = new PropertyMetadata('borderRadius');
  var borderTopLeftRadius;
  var borderTopLeftRadius_metadata = new PropertyMetadata('borderTopLeftRadius');
  var borderTopRightRadius;
  var borderTopRightRadius_metadata = new PropertyMetadata('borderTopRightRadius');
  var borderBottomLeftRadius;
  var borderBottomLeftRadius_metadata = new PropertyMetadata('borderBottomLeftRadius');
  var borderBottomRightRadius;
  var borderBottomRightRadius_metadata = new PropertyMetadata('borderBottomRightRadius');
  var borderStyle;
  var borderStyle_metadata = new PropertyMetadata('borderStyle');
  var borderTopStyle;
  var borderTopStyle_metadata = new PropertyMetadata('borderTopStyle');
  var borderRightStyle;
  var borderRightStyle_metadata = new PropertyMetadata('borderRightStyle');
  var borderBottomStyle;
  var borderBottomStyle_metadata = new PropertyMetadata('borderBottomStyle');
  var borderLeftStyle;
  var borderLeftStyle_metadata = new PropertyMetadata('borderLeftStyle');
  var borderWidth;
  var borderWidth_metadata = new PropertyMetadata('borderWidth');
  var borderTopWidth;
  var borderTopWidth_metadata = new PropertyMetadata('borderTopWidth');
  var borderRightWidth;
  var borderRightWidth_metadata = new PropertyMetadata('borderRightWidth');
  var borderBottomWidth;
  var borderBottomWidth_metadata = new PropertyMetadata('borderBottomWidth');
  var borderLeftWidth;
  var borderLeftWidth_metadata = new PropertyMetadata('borderLeftWidth');
  var borderColor;
  var borderColor_metadata = new PropertyMetadata('borderColor');
  var borderTopColor;
  var borderTopColor_metadata = new PropertyMetadata('borderTopColor');
  var borderRightColor;
  var borderRightColor_metadata = new PropertyMetadata('borderRightColor');
  var borderBottomColor;
  var borderBottomColor_metadata = new PropertyMetadata('borderBottomColor');
  var borderLeftColor;
  var borderLeftColor_metadata = new PropertyMetadata('borderLeftColor');
  var bottom;
  var bottom_metadata = new PropertyMetadata('bottom');
  var boxSizing;
  var boxSizing_metadata = new PropertyMetadata('boxSizing');
  function boxShadow$lambda() {
    return new BoxShadows();
  }
  var boxShadow;
  var boxShadow_metadata = new PropertyMetadata('boxShadow');
  var clear;
  var clear_metadata = new PropertyMetadata('clear');
  var color;
  var color_metadata = new PropertyMetadata('color');
  var columnGap;
  var columnGap_metadata = new PropertyMetadata('columnGap');
  var contain;
  var contain_metadata = new PropertyMetadata('contain');
  var content;
  var content_metadata = new PropertyMetadata('content');
  var cursor;
  var cursor_metadata = new PropertyMetadata('cursor');
  var direction;
  var direction_metadata = new PropertyMetadata('direction');
  var display;
  var display_metadata = new PropertyMetadata('display');
  var filter;
  var filter_metadata = new PropertyMetadata('filter');
  var flexDirection;
  var flexDirection_metadata = new PropertyMetadata('flexDirection');
  var flexGrow;
  var flexGrow_metadata = new PropertyMetadata('flexGrow');
  var flexShrink;
  var flexShrink_metadata = new PropertyMetadata('flexShrink');
  var flexBasis;
  var flexBasis_metadata = new PropertyMetadata('flexBasis');
  var flexWrap;
  var flexWrap_metadata = new PropertyMetadata('flexWrap');
  var float;
  var float_metadata = new PropertyMetadata('float');
  var fontFamily;
  var fontFamily_metadata = new PropertyMetadata('fontFamily');
  var fontSize;
  var fontSize_metadata = new PropertyMetadata('fontSize');
  var fontWeight;
  var fontWeight_metadata = new PropertyMetadata('fontWeight');
  var fontStyle;
  var fontStyle_metadata = new PropertyMetadata('fontStyle');
  var gap;
  var gap_metadata = new PropertyMetadata('gap');
  var gridAutoColumns;
  var gridAutoColumns_metadata = new PropertyMetadata('gridAutoColumns');
  var gridAutoFlow;
  var gridAutoFlow_metadata = new PropertyMetadata('gridAutoFlow');
  var gridAutoRows;
  var gridAutoRows_metadata = new PropertyMetadata('gridAutoRows');
  var gridColumn;
  var gridColumn_metadata = new PropertyMetadata('gridColumn');
  var gridColumnEnd;
  var gridColumnEnd_metadata = new PropertyMetadata('gridColumnEnd');
  var gridColumnGap;
  var gridColumnGap_metadata = new PropertyMetadata('gridColumnGap');
  var gridColumnStart;
  var gridColumnStart_metadata = new PropertyMetadata('gridColumnStart');
  var gridGap;
  var gridGap_metadata = new PropertyMetadata('gridGap');
  var gridRow;
  var gridRow_metadata = new PropertyMetadata('gridRow');
  var gridRowEnd;
  var gridRowEnd_metadata = new PropertyMetadata('gridRowEnd');
  var gridRowGap;
  var gridRowGap_metadata = new PropertyMetadata('gridRowGap');
  var gridRowStart;
  var gridRowStart_metadata = new PropertyMetadata('gridRowStart');
  var gridTemplate;
  var gridTemplate_metadata = new PropertyMetadata('gridTemplate');
  var gridTemplateAreas;
  var gridTemplateAreas_metadata = new PropertyMetadata('gridTemplateAreas');
  var gridTemplateColumns;
  var gridTemplateColumns_metadata = new PropertyMetadata('gridTemplateColumns');
  var gridTemplateRows;
  var gridTemplateRows_metadata = new PropertyMetadata('gridTemplateRows');
  var height;
  var height_metadata = new PropertyMetadata('height');
  var hyphens;
  var hyphens_metadata = new PropertyMetadata('hyphens');
  var justifyContent;
  var justifyContent_metadata = new PropertyMetadata('justifyContent');
  var left;
  var left_metadata = new PropertyMetadata('left');
  var letterSpacing;
  var letterSpacing_metadata = new PropertyMetadata('letterSpacing');
  var lineHeight;
  var lineHeight_metadata = new PropertyMetadata('lineHeight');
  var listStyleType;
  var listStyleType_metadata = new PropertyMetadata('listStyleType');
  var margin;
  var margin_metadata = new PropertyMetadata('margin');
  var marginTop;
  var marginTop_metadata = new PropertyMetadata('marginTop');
  var marginRight;
  var marginRight_metadata = new PropertyMetadata('marginRight');
  var marginBottom;
  var marginBottom_metadata = new PropertyMetadata('marginBottom');
  var marginLeft;
  var marginLeft_metadata = new PropertyMetadata('marginLeft');
  var minWidth;
  var minWidth_metadata = new PropertyMetadata('minWidth');
  var maxWidth;
  var maxWidth_metadata = new PropertyMetadata('maxWidth');
  var minHeight;
  var minHeight_metadata = new PropertyMetadata('minHeight');
  var maxHeight;
  var maxHeight_metadata = new PropertyMetadata('maxHeight');
  var objectFit;
  var objectFit_metadata = new PropertyMetadata('objectFit');
  var objectPosition;
  var objectPosition_metadata = new PropertyMetadata('objectPosition');
  var opacity;
  var opacity_metadata = new PropertyMetadata('opacity');
  var outline;
  var outline_metadata = new PropertyMetadata('outline');
  var overflow;
  var overflow_metadata = new PropertyMetadata('overflow');
  var overflowX;
  var overflowX_metadata = new PropertyMetadata('overflowX');
  var overflowY;
  var overflowY_metadata = new PropertyMetadata('overflowY');
  var overflowWrap;
  var overflowWrap_metadata = new PropertyMetadata('overflowWrap');
  var overscrollBehavior;
  var overscrollBehavior_metadata = new PropertyMetadata('overscrollBehavior');
  var padding;
  var padding_metadata = new PropertyMetadata('padding');
  var paddingTop;
  var paddingTop_metadata = new PropertyMetadata('paddingTop');
  var paddingRight;
  var paddingRight_metadata = new PropertyMetadata('paddingRight');
  var paddingBottom;
  var paddingBottom_metadata = new PropertyMetadata('paddingBottom');
  var paddingLeft;
  var paddingLeft_metadata = new PropertyMetadata('paddingLeft');
  var pointerEvents;
  var pointerEvents_metadata = new PropertyMetadata('pointerEvents');
  var position;
  var position_metadata = new PropertyMetadata('position');
  var right;
  var right_metadata = new PropertyMetadata('right');
  var rowGap;
  var rowGap_metadata = new PropertyMetadata('rowGap');
  var scrollBehavior;
  var scrollBehavior_metadata = new PropertyMetadata('scrollBehavior');
  var textAlign;
  var textAlign_metadata = new PropertyMetadata('textAlign');
  var textDecoration;
  var textDecoration_metadata = new PropertyMetadata('textDecoration');
  var textOverflow;
  var textOverflow_metadata = new PropertyMetadata('textOverflow');
  var textTransform;
  var textTransform_metadata = new PropertyMetadata('textTransform');
  var top;
  var top_metadata = new PropertyMetadata('top');
  function transform$lambda() {
    return new Transforms();
  }
  var transform;
  var transform_metadata = new PropertyMetadata('transform');
  function transition$lambda() {
    return new Transitions();
  }
  var transition;
  var transition_metadata = new PropertyMetadata('transition');
  var verticalAlign;
  var verticalAlign_metadata = new PropertyMetadata('verticalAlign');
  var visibility;
  var visibility_metadata = new PropertyMetadata('visibility');
  var whiteSpace;
  var whiteSpace_metadata = new PropertyMetadata('whiteSpace');
  var width;
  var width_metadata = new PropertyMetadata('width');
  var wordBreak;
  var wordBreak_metadata = new PropertyMetadata('wordBreak');
  var wordWrap;
  var wordWrap_metadata = new PropertyMetadata('wordWrap');
  var userSelect;
  var userSelect_metadata = new PropertyMetadata('userSelect');
  var tableLayout;
  var tableLayout_metadata = new PropertyMetadata('tableLayout');
  var borderCollapse;
  var borderCollapse_metadata = new PropertyMetadata('borderCollapse');
  var zIndex;
  var zIndex_metadata = new PropertyMetadata('zIndex');
  var IterationCount$Companion_instance = null;
  var AnimationDirection$initial_instance;
  var AnimationDirection$inherit_instance;
  var AnimationDirection$unset_instance;
  var AnimationDirection$normal_instance;
  var AnimationDirection$reverse_instance;
  var AnimationDirection$alternate_instance;
  var AnimationDirection$alternateReverse_instance;
  var FillMode$initial_instance;
  var FillMode$inherit_instance;
  var FillMode$unset_instance;
  var FillMode$none_instance;
  var FillMode$forwards_instance;
  var FillMode$backwards_instance;
  var FillMode$both_instance;
  var PlayState$initial_instance;
  var PlayState$inherit_instance;
  var PlayState$unset_instance;
  var PlayState$running_instance;
  var PlayState$paused_instance;
  function Animations() {
    Animations$Companion_getInstance();
    StyleList.call(this, ', ');
  }
  function Animations$Companion() {
    Animations$Companion_instance = this;
    this.none = new Animations();
  }
  Animations$Companion.$metadata$ = {kind: Kind_OBJECT, simpleName: 'Companion', interfaces: []};
  var Animations$Companion_instance = null;
  function Animations$Companion_getInstance() {
    if (Animations$Companion_instance === null) {
      new Animations$Companion();
    }return Animations$Companion_instance;
  }
  Animations.$metadata$ = {kind: Kind_CLASS, simpleName: 'Animations', interfaces: [StyleList]};
  function BoxShadows() {
    BoxShadows$Companion_getInstance();
    StyleList.call(this, ', ');
  }
  function BoxShadows$Companion() {
    BoxShadows$Companion_instance = this;
    this.none = new BoxShadows();
  }
  BoxShadows$Companion.$metadata$ = {kind: Kind_OBJECT, simpleName: 'Companion', interfaces: []};
  var BoxShadows$Companion_instance = null;
  function BoxShadows$Companion_getInstance() {
    if (BoxShadows$Companion_instance === null) {
      new BoxShadows$Companion();
    }return BoxShadows$Companion_instance;
  }
  BoxShadows.$metadata$ = {kind: Kind_CLASS, simpleName: 'BoxShadows', interfaces: [StyleList]};
  var LineHeight$Companion_instance = null;
  var TextDecorationLine$initial_instance;
  var TextDecorationLine$inherit_instance;
  var TextDecorationLine$unset_instance;
  var TextDecorationLine$underline_instance;
  var TextDecorationLine$overline_instance;
  var TextDecorationLine$lineThrough_instance;
  var TextDecoration$Companion_instance = null;
  var TextDecorationStyle$initial_instance;
  var TextDecorationStyle$inherit_instance;
  var TextDecorationStyle$unset_instance;
  var TextDecorationStyle$solid_instance;
  var TextDecorationStyle$double_instance;
  var TextDecorationStyle$dotted_instance;
  var TextDecorationStyle$dashed_instance;
  var TextDecorationStyle$wavy_instance;
  var Timing$Companion_instance = null;
  var TransitionDirection$initial_instance;
  var TransitionDirection$inherit_instance;
  var TransitionDirection$unset_instance;
  var TransitionDirection$start_instance;
  var TransitionDirection$end_instance;
  function Transforms() {
    Transforms$Companion_getInstance();
    StyleList.call(this, ' ');
  }
  function Transforms$Companion() {
    Transforms$Companion_instance = this;
    this.none = new Transforms();
  }
  Transforms$Companion.$metadata$ = {kind: Kind_OBJECT, simpleName: 'Companion', interfaces: []};
  var Transforms$Companion_instance = null;
  function Transforms$Companion_getInstance() {
    if (Transforms$Companion_instance === null) {
      new Transforms$Companion();
    }return Transforms$Companion_instance;
  }
  Transforms.$metadata$ = {kind: Kind_CLASS, simpleName: 'Transforms', interfaces: [StyleList]};
  function Transitions() {
    Transitions$Companion_getInstance();
    StyleList.call(this, ', ');
  }
  function Transitions$Companion() {
    Transitions$Companion_instance = this;
    this.none = new Transitions();
  }
  Transitions$Companion.$metadata$ = {kind: Kind_OBJECT, simpleName: 'Companion', interfaces: []};
  var Transitions$Companion_instance = null;
  function Transitions$Companion_getInstance() {
    if (Transitions$Companion_instance === null) {
      new Transitions$Companion();
    }return Transitions$Companion_instance;
  }
  Transitions.$metadata$ = {kind: Kind_CLASS, simpleName: 'Transitions', interfaces: [StyleList]};
  var package$kotlinx = _.kotlinx || (_.kotlinx = {});
  var package$css = package$kotlinx.css || (package$kotlinx.css = {});
  package$css.StyleList = StyleList;
  var package$properties = package$css.properties || (package$css.properties = {});
  Object.defineProperty(Animations, 'Companion', {get: Animations$Companion_getInstance});
  package$properties.Animations = Animations;
  Object.defineProperty(BoxShadows, 'Companion', {get: BoxShadows$Companion_getInstance});
  package$properties.BoxShadows = BoxShadows;
  Object.defineProperty(Transforms, 'Companion', {get: Transforms$Companion_getInstance});
  package$properties.Transforms = Transforms;
  Object.defineProperty(Transitions, 'Companion', {get: Transitions$Companion_getInstance});
  package$properties.Transitions = Transitions;
  hyphenize = memoizeString(hyphenize$lambda);
  ZERO = '0';
  alignContent = new CSSProperty();
  alignItems = new CSSProperty();
  alignSelf = new CSSProperty();
  animation = new CSSProperty(animation$lambda);
  background = new CSSProperty();
  backgroundAttachment = new CSSProperty();
  backgroundClip = new CSSProperty();
  backgroundColor = new CSSProperty();
  backgroundImage = new CSSProperty();
  backgroundOrigin = new CSSProperty();
  backgroundPosition = new CSSProperty();
  backgroundRepeat = new CSSProperty();
  backgroundSize = new CSSProperty();
  border = new CSSProperty();
  borderTop = new CSSProperty();
  borderRight = new CSSProperty();
  borderBottom = new CSSProperty();
  borderLeft = new CSSProperty();
  borderSpacing = new CSSProperty();
  borderRadius = new CSSProperty();
  borderTopLeftRadius = new CSSProperty();
  borderTopRightRadius = new CSSProperty();
  borderBottomLeftRadius = new CSSProperty();
  borderBottomRightRadius = new CSSProperty();
  borderStyle = new CSSProperty();
  borderTopStyle = new CSSProperty();
  borderRightStyle = new CSSProperty();
  borderBottomStyle = new CSSProperty();
  borderLeftStyle = new CSSProperty();
  borderWidth = new CSSProperty();
  borderTopWidth = new CSSProperty();
  borderRightWidth = new CSSProperty();
  borderBottomWidth = new CSSProperty();
  borderLeftWidth = new CSSProperty();
  borderColor = new CSSProperty();
  borderTopColor = new CSSProperty();
  borderRightColor = new CSSProperty();
  borderBottomColor = new CSSProperty();
  borderLeftColor = new CSSProperty();
  bottom = new CSSProperty();
  boxSizing = new CSSProperty();
  boxShadow = new CSSProperty(boxShadow$lambda);
  clear = new CSSProperty();
  color = new CSSProperty();
  columnGap = new CSSProperty();
  contain = new CSSProperty();
  content = new CSSProperty();
  cursor = new CSSProperty();
  direction = new CSSProperty();
  display = new CSSProperty();
  filter = new CSSProperty();
  flexDirection = new CSSProperty();
  flexGrow = new CSSProperty();
  flexShrink = new CSSProperty();
  flexBasis = new CSSProperty();
  flexWrap = new CSSProperty();
  float = new CSSProperty();
  fontFamily = new CSSProperty();
  fontSize = new CSSProperty();
  fontWeight = new CSSProperty();
  fontStyle = new CSSProperty();
  gap = new CSSProperty();
  gridAutoColumns = new CSSProperty();
  gridAutoFlow = new CSSProperty();
  gridAutoRows = new CSSProperty();
  gridColumn = new CSSProperty();
  gridColumnEnd = new CSSProperty();
  gridColumnGap = new CSSProperty();
  gridColumnStart = new CSSProperty();
  gridGap = new CSSProperty();
  gridRow = new CSSProperty();
  gridRowEnd = new CSSProperty();
  gridRowGap = new CSSProperty();
  gridRowStart = new CSSProperty();
  gridTemplate = new CSSProperty();
  gridTemplateAreas = new CSSProperty();
  gridTemplateColumns = new CSSProperty();
  gridTemplateRows = new CSSProperty();
  height = new CSSProperty();
  hyphens = new CSSProperty();
  justifyContent = new CSSProperty();
  left = new CSSProperty();
  letterSpacing = new CSSProperty();
  lineHeight = new CSSProperty();
  listStyleType = new CSSProperty();
  margin = new CSSProperty();
  marginTop = new CSSProperty();
  marginRight = new CSSProperty();
  marginBottom = new CSSProperty();
  marginLeft = new CSSProperty();
  minWidth = new CSSProperty();
  maxWidth = new CSSProperty();
  minHeight = new CSSProperty();
  maxHeight = new CSSProperty();
  objectFit = new CSSProperty();
  objectPosition = new CSSProperty();
  opacity = new CSSProperty();
  outline = new CSSProperty();
  overflow = new CSSProperty();
  overflowX = new CSSProperty();
  overflowY = new CSSProperty();
  overflowWrap = new CSSProperty();
  overscrollBehavior = new CSSProperty();
  padding = new CSSProperty();
  paddingTop = new CSSProperty();
  paddingRight = new CSSProperty();
  paddingBottom = new CSSProperty();
  paddingLeft = new CSSProperty();
  pointerEvents = new CSSProperty();
  position = new CSSProperty();
  right = new CSSProperty();
  rowGap = new CSSProperty();
  scrollBehavior = new CSSProperty();
  textAlign = new CSSProperty();
  textDecoration = new CSSProperty();
  textOverflow = new CSSProperty();
  textTransform = new CSSProperty();
  top = new CSSProperty();
  transform = new CSSProperty(transform$lambda);
  transition = new CSSProperty(transition$lambda);
  verticalAlign = new CSSProperty();
  visibility = new CSSProperty();
  whiteSpace = new CSSProperty();
  width = new CSSProperty();
  wordBreak = new CSSProperty();
  wordWrap = new CSSProperty();
  userSelect = new CSSProperty();
  tableLayout = new CSSProperty();
  borderCollapse = new CSSProperty();
  zIndex = new CSSProperty();
  return _;
}(module.exports, require('kotlin')));

//# sourceMappingURL=kotlin-css-js.js.map
