import kotlin.js.Json
import kotlin.js.json

external fun require(module: String): dynamic

external val process: dynamic
external val __dirname: dynamic



fun main() {
    var numOfUsers = 0
    println("Server Starting!")

    val express = require("express")
    val app = express()
    val path = require("path")
    val bodyParser = require("body-parser")
    val debug = require("debug")("kotlin_react_chat:server")

    /**
     * Get port from environment and store in Express.
     */

    val port = normalizePort(process.env.PORT)
    app.use(bodyParser.json())
    app.set("port", port)

    // view engine setup
    app.set("views", path.join(__dirname, "../../webapp"))
    app.set("view engine", "ejs")
    app.use(express.static("webapp"))
    app.use("/", router())

    val server = require("http").createServer(app)
    server.listen(port){
        println("Chat Server listening on port http://localhost:$port")
    }

    val io     = require("socket.io")(server)
    val users = arrayListOf<User>()

    io.on("connect") { socket ->
        console.log("socket connection")
        socket.on("join") { nickName,callback ->
            socket.nickname = nickName
            val succes = users.addUser(socket.id,nickName)
            if (succes.equals(USER_FOUND)) return@on callback(USER_FOUND,false)
            numOfUsers = users.size
            console.log(numOfUsers)
            val usersList = getUserList(users)
//            usersList.add(json("index" to socket.id ,
//                                      "nickName" to nickName))
            console.log(usersList.toJSON())
            val userJoinedData = getJsonRespose(numOfUsers,nickName,usersList)

            callback("",true) //succes login open chatwindow
            socket.emit("loginUser", userJoinedData)

            socket.broadcast.emit("joinUser", userJoinedData)
        }

        socket.on("disconnect") {
            val succes = users.removeUser(socket.id)

            console.log("disconnect:$succes")
            numOfUsers = users.size
            if (succes) {
                console.log("${socket.nickname} disconected")
                val usersList = getUserList(users)
                val userJoinedData = getJsonRespose(numOfUsers,socket.nickname,usersList)
                socket.broadcast.emit("user_left", userJoinedData)
            }
        }

        socket.on("new_message") { msg ->
            val user = users.getUser(socket.id)
            val userJoinedData =  json("nickName" to user.nickname,
                                             "message" to msg)
            socket.broadcast.emit("new_message", userJoinedData)
        }
    }

}

fun getJsonRespose(numOfUsers: Int, nickName: dynamic, usersList: ArrayList<Json>): dynamic {
    return json("numOfUsers" to numOfUsers,
            "nickName"  to nickName,
            "usersList" to usersList)
}

fun normalizePort(port: Int): Int = if (port >= 0) port else 8000