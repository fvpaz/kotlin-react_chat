import org.jetbrains.kotlin.gradle.tasks.*
import org.jetbrains.kotlin.gradle.dsl.*

group = "org.example"
version = "1.0.0"

repositories {}
val reactVersion    = "16.13.1"
dependencies {
    implementation(kotlin("stdlib-js"))
//    implementation("org.jetbrains.kotlinx:kotlinx-nodejs:0.0.3")
    implementation(npm("react", "$reactVersion"))
    implementation(npm("react-dom", "$reactVersion"))
    implementation(npm("socket.io", "^2.3.0"))
    implementation(npm("body-parser", "^1.19.0"))
    implementation(npm("debug", "^4.1.1"))
    implementation(npm("ejs", "^3.1.3"))
    implementation(npm("express", "^4.17.1"))
//    implementation(npm("kotlin", "1.4-M2"))
//    implementation(npm("styled-components"))
//    implementation(npm("inline-style-prefixer"))
}

kotlin {
    js{
        nodejs { }
        binaries.executable()
    }

    sourceSets["main"].kotlin.srcDirs("src")
    sourceSets["main"].resources.srcDirs("resources")
}

//tasks.compileKotlinJs{
//    getkotlinOptions(kotlinOptions)
//}

tasks {
    named("compileKotlinJs", Kotlin2JsCompile::class) {
        getkotlinOptions(kotlinOptions)
    }
}

fun getkotlinOptions(kotlinOptions: KotlinJsOptions) {
    kotlinOptions.outputFile = "${projectDir}/server/node.js"
    kotlinOptions.moduleKind = "commonjs"
    kotlinOptions.sourceMap = true
}