import org.jetbrains.kotlin.gradle.dsl.KotlinJsOptions
import org.jetbrains.kotlin.gradle.tasks.*

group = "org.example"
version = "1.0-SNAPSHOT"

plugins {
    id("org.jetbrains.kotlin.js") version "1.4.0-rc"
}

allprojects {
    repositories {
        maven("https://dl.bintray.com/kotlin/kotlin-eap")
        mavenCentral()
        jcenter()
        maven("https://kotlin.bintray.com/kotlinx")
    }
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.js")
}

kotlin {
    js {
        browser { }
        binaries.executable()
    }
}

dependencies {
    implementation(kotlin("stdlib-js"))
}

tasks {
    named("compileKotlinJs", Kotlin2JsCompile::class) {
        getkotlinOptions(kotlinOptions)
    }
}

//tasks.compileKotlinJs{
//    getkotlinOptions(kotlinOptions)
//}

fun getkotlinOptions(kotlinOptions: KotlinJsOptions) {
    kotlinOptions.metaInfo = true
    kotlinOptions.outputFile = "node/app.js"
    //kotlinOptions.main = "call"
//    kotlinOptions.moduleKind = "commonjs"

    // these two options enable browser debugging
    // disable for production to reduce file size
    kotlinOptions.sourceMap = true
    //kotlinOptions.sourceMapEmbedSources = "always"
}

fun commonRepoHandler(handler: RepositoryHandler.() -> Unit) {}
fun commonRepos() = commonRepoHandler {
    maven("https://dl.bintray.com/kotlin/kotlin-eap")
    jcenter()
    mavenCentral()
}