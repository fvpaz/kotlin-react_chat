group = "org.example"
version = "1.0-SNAPSHOT"

plugins {
    id("org.jetbrains.kotlin.js") version "1.4-M1"
}

repositories {
    maven("https://dl.bintray.com/kotlin/kotlin-eap")
    jcenter()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-js"))
}

kotlin {
    target {
        browser{}
    }

    sourceSets["main"].kotlin.srcDirs("src")
    sourceSets["main"].resources.srcDirs("resources")
}

lateinit var destDirKotlinJs: File
tasks.compileKotlinJs{
    kotlinOptions.outputFile = "${projectDir}/server/app.js"
//    kotlinOptions.metaInfo = true
    kotlinOptions.moduleKind = "commonjs"
    // these two options enable browser debugging
    // disable for production to reduce file size
    kotlinOptions.sourceMap = true
//    kotlinOptions.sourceMapEmbedSources = "always"

//    destDirKotlinJs  = destinationDir
}

//val outputDir = "${projectDir}/../node_modules/common"
//
//tasks.register<Copy>("assembleJs"){
//    from(destDirKotlinJs)
//    println(destDirKotlinJs)
//    into(outputDir)
//    dependsOn(tasks.mainClasses)
//}
//
//tasks.register<Copy>("copyResources"){
//    from("${projectDir}/package.json")
//    into(outputDir)
//    dependsOn(tasks.mainClasses)
//}
//
//tasks.assemble{
//    dependsOn(tasks["assembleJs"])
//    dependsOn(tasks["copyResources"])
//}